# Event-house

Social media for musical events

![alt text](./src/Assets/readMe1.jpg)

Used Technologies: React, Redux, Firebase, Bootstrap, Material-UI

# Functionalities

* signup/login/find user
* add/follow event
* search for events by multiple criteria
* show event details, find event in google maps
* add/delete/like post to profile/event
* add/delete comment to post
* add user avatar/mood
* message users

![alt text](./src/Assets/readMe4.jpg)

![alt text](./src/Assets/readMe2.jpg)

![alt text](./src/Assets/readMe3.jpg)
# Installation
1. Clone the repo
2. npm install
3. npm start
 

 credentials for better experience


    email: lora.hristova@abv.bg
    password: 123456

4. Enjoy



