import { useEffect, useState } from "react";
import { Button, Col, Container, Row, Card } from "react-bootstrap";
import { CameraFill } from "react-bootstrap-icons";
import { useNavigate, useParams } from "react-router-dom";
import TextField from "@mui/material/TextField";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import InputAdornment from "@mui/material/InputAdornment";
import { Dropdown } from "react-bootstrap";

import AvatarModal from "../../Layouts/Modals/AvatarModal/AvatarModal";
import MoodModal from "../../Layouts/Modals/MoodModal/MoodModal";
import PostModal from "../../Layouts/Modals/PostModal/PostModal";
import { fromPostsDocument, getLivePosts } from "../../Services/posts.Service";
import Post from "../../Common/Post/Post";
import {
  fromUsersDocument,
  getLiveUsers,
  getUserByUsername,
} from "../../Services/users.Service";
import modalActions from '../../actions/modal';
import Messages from "../Messages/Messages";
import { showAvatarSelecter, showMessageSelecter, showMoodSelecter, showPostSelecter } from "../../reducers/modal";
import { userDataSelector } from "../../reducers/user";
import userActions from '../../actions/user'
import "./Profile.css";

const Profile = ({ showAvatarModal, showMoodModal, showPostModal, showMessageModal, showMes, userData }) => {
  const navigate = useNavigate();
  const { userName } = useParams();
  const [user, setUser] = useState(null);
  const [messageUser, setMessageUser] = useState(null);
  const [posts, setPosts] = useState([]);
  const [users, setUsers] = useState([]);
  const [suggestions, setSuggestions] = useState([]);

  const handleShowAvatarModal = () => showAvatarModal({ showAvatar: true });
  const handleShowMoodModal = () => showMoodModal({ showMood: true });
  const handleShowPostModal = () => showPostModal({ showPost: true })
  const handleShowMessages = () => {
    setMessageUser(user);
    showMessageModal()
  };

  useEffect(() => {
    getUserByUsername(userName).then((snapshot) => {
      setUser(snapshot.val());
    }, []);

    const unsubscribe = getLivePosts((snapshot) => {
      setPosts(filterPosts(fromPostsDocument(snapshot)));
    });

    return () => unsubscribe();
  }, [userName, userData]);

  useEffect(() => {
    const unsubscribe = getLiveUsers((snapshot) => {
      setUsers(fromUsersDocument(snapshot));
    });

    return () => unsubscribe();
  }, []);

  const filterPosts = (result) => {
    const filteredPosts = result
      .filter((post) => post.author === userName)
      .reverse();
    return filteredPosts;
  };

  const attachUser = (text) => {
    let matches = [];

    if (text.length > 0) {
      matches = users.filter((user) => {
        const regex = new RegExp(`${text}`, "gi");
        return user.userName.match(regex);
      });
    }

    setSuggestions(matches);
  };

  return (
    <Container className="profile-container">
      {user && userData && (
        <>
          <Row>
            <Col xs="4" md="1"></Col>
            {user.userName === userData.userName ? (
              <Col xs="9" md="12">
                <img
                  src={userData.userAvatar}
                  alt="user Avatar"
                  className="avatar"
                />
                <span onClick={handleShowAvatarModal}>
                  <CameraFill className="iconStyle2" />
                </span>
                <span className="fw-bold mood">{userData.mood}</span>)
                <span className="fw-bold name d-flex align-items-end">
                  {userData.userName}
                </span>
                <span className="fw-bold buttons d-flex align-items-end">
                  <Button
                    className="pull-right button-style"
                    variant="secondary"
                    onClick={handleShowMoodModal}
                  >
                    Add mood
                  </Button>
                  <Button
                    className="pull-right button-style"
                    variant="secondary"
                    onClick={handleShowPostModal}
                  >
                    Add post
                  </Button>
                </span>
              </Col>
            ) : (
              <Col xs="9" md="12">
                <img
                  src={user.userAvatar}
                  alt="user Avatar"
                  className="avatar"
                />
                <span className="fw-bold mood">{user.mood}</span>
                )
                <span className="fw-bold name d-flex align-items-end">
                  {user.userName}
                </span>
                <span className="fw-bold buttons d-flex align-items-end">
                  <Button
                    className="pull-right button-style"
                    variant="secondary"
                    onClick={handleShowMessages}
                  >
                    Message
                  </Button>
                </span>
              </Col>
            )}
            <Col xs="4" md="1"></Col>
          </Row>
        </>
      )}

      <AvatarModal user={user} />
      <MoodModal />
      <PostModal />

      <Row>
        <Col>
          {userData ? (
            <Card className="usersCard">
              <Card.Body>
                <TextField
                  label="Find person"
                  id="outlined-start-adornment"
                  sx={{ m: 1, width: "94%" }}
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start"></InputAdornment>
                    ),
                  }}
                  onChange={(e) => attachUser(e.target.value)}
                />

                {suggestions.length > 0
                  ? suggestions.map((suggestion, key) => (
                      <Dropdown.Item
                        key={key}
                        eventKey={suggestion.userName}
                        onClick={() =>
                          navigate(`/profile/${suggestion.userName}`)
                        }
                      >
                        {suggestion.userName}
                      </Dropdown.Item>
                    ))
                  : users.map((user, key) => (
                      <div
                        className="usersList"
                        key={key}
                        onClick={() => navigate(`/profile/${user.userName}`)}
                      >
                        {user.userName}
                      </div>
                    ))}
              </Card.Body>
            </Card>
          ) : (
            <div className="empty"></div>
          )}
        </Col>
        <Col xs="1" md="9">
          <div className="posts-container">
            {posts && user && userData && (
              <>
                {posts.length > 0 ? (
                  posts.map((post, key) => {
                    let avatar = user.userAvatar;
                    if (user.userName === userData.userName) {
                      avatar = userData.userAvatar;
                    }
                    return <Post key={key} post={post} userAvatar={avatar} />;
                  })
                ) : (
                  <p className="fw-bold no-posts">
                    No posts to show
                  </p>
                )}
              </>
            )}
          </div>
        </Col>
      </Row>
      {messageUser && userData && showMes && (
        <Messages
          recipientName={messageUser.userName}
          userName={userData.userName}
        />
      )}
    </Container>
  );
}

export default connect(
  (state) => ({
      showAvatar: showAvatarSelecter(state),
      showMood: showMoodSelecter(state),
      showPost: showPostSelecter(state),
      showMes: showMessageSelecter(state),
      userData: userDataSelector(state)
  }),
  (dispatch) =>
      bindActionCreators(
          {
              ...modalActions, ...userActions
          },
          dispatch
      )
)(Profile);
