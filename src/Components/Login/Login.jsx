import { useState } from "react";
import { Link, useNavigate } from "react-router-dom";
import { Card, Form, Button, Alert, Container } from "react-bootstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { loginUser } from "../../Services/auth.Service";
import { getUserData } from "../../Services/users.Service";
import userActions from "../../actions/user";
import { userSelector } from "../../reducers/user";
import "./Login.css";

const Login = ({ setUser, setUserData }) => {
  const [userL, setUserL] = useState({
    email: "",
    password: "",
  });
  const [error, setError] = useState("");
  const navigate = useNavigate();

  const updateForm = (prop) => (e) => {
    setUserL({
      ...userL,
      [prop]: e.target.value,
    });
  };

  async function handleLogin(e) {
    e.preventDefault();

    try {
      await loginUser(userL.email, userL.password)
        .then(async (u) => {
          const snapshot = await getUserData(u.user.uid);
          if (!snapshot.exists()) {
            return setError("User does not exist :(");
          }
          if (snapshot.exists()) {
            setUser({ user: u.user });
            setUserData({
              userData: snapshot.val()[Object.keys(snapshot.val())[0]],
            });
            navigate("../home", { replace: true });
          }
        })
        .catch((error) => setError(`${error}`));
    } catch (error) {
      setError(`${error.message}`);
    }
  }

  return (
    <Container className="d-flex align-items-center justify-content-center container-login">
      <div className="w-100 content-width">
        <Card>
          <Card.Body>
            <h2 className="text-center mb-4">Log In</h2>
            {error && <Alert variant="danger">{error}</Alert>}
            <Form>
              <Form.Group id="email">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  value={userL.email}
                  onChange={updateForm("email")}
                  required
                />
              </Form.Group>
              <Form.Group id="password">
                <Form.Label className="mt-2">Password</Form.Label>
                <Form.Control
                  type="password"
                  value={userL.password}
                  onChange={updateForm("password")}
                  required
                />
              </Form.Group>
              <Button
                className="w-100 mt-4"
                style={{ backgroundColor: "#696969" }}
                onClick={handleLogin}
                type="submit"
              >
                Log In
              </Button>
            </Form>
          </Card.Body>
        </Card>
        <div className="w-100 text-center mt-2 link-style">
          Don't have an account ?{" "}
          <Link to="/signup" className="link-style">
            Sign up
          </Link>
        </div>
      </div>
    </Container>
  );
};

export default connect(
  (state) => ({
    user: userSelector(state),
  }),
  (dispatch) =>
    bindActionCreators(
      {
        ...userActions,
      },
      dispatch
    )
)(Login);
