import { defineDateAndTime } from "../../../Common/helper";
import "./MessageBox.css";

export const MessageBox = ({ text, sentByYou, timeStamp }) => {
  const dateTime = defineDateAndTime(new Date(timeStamp));
  return (
    <div className={sentByYou ? "sent" : "received"}>
      <p>{text}</p>
      <span className="time-size">
        {dateTime.day} {dateTime.time}
      </span>
    </div>
  );
};
