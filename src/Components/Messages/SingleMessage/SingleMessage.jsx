import { MessageBox } from "../MessageBox/MessageBox";
import { useEffect, useRef } from "react";
import { XLg } from "react-bootstrap-icons";
import React, { useState } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import modalActions from '../../../actions/modal'
import { addMessage } from "../../../Services/messages.Service";
import { getUserByUsername } from "../../../Services/users.Service";
import { showMessageSelecter } from "../../../reducers/modal";
import { userDataSelector } from "../../../reducers/user";
import userActions from '../../../actions/user'
import "./SingleMessage.css";

const SingleMessage = ({ messages, recipientName, hideMessageModal, userData }) => {
  const [recipient, setRecipient] = useState(null);
  const [newMessageText, setNewMessageText] = useState(null);
  const [text, setText] = useState("");

  useEffect(() => {
    getUserByUsername(recipientName).then((snapshot) => {
      setRecipient(snapshot.val());
    });
  }, [recipientName]);

  useEffect(() => {
    scrollToBottom();
  }, [messages]);

  const messagesEndRef = useRef(null);

  const scrollToBottom = () => {
    messagesEndRef.current?.scrollIntoView({ behavior: "smooth" });
  };

  const closeMessageBar = () => hideMessageModal();

  const handleInput = () => (e) => {
    e.preventDefault();
    setText(e.target.value);
    setNewMessageText({
      text: e.target.value,
      timeStamp: new Date().valueOf(),
    });
  };

  const createMessage = () => (e) => {
    if (e.key === "Enter") {
      e.preventDefault();
      addMessage(userData.userName, recipient.userName, newMessageText);
      setText("");
    }
  };

  return (
    <div className="message-box-container">
      <div className="message-box-title">
        <div>
          {recipient && (
            <div className="be-comment-author">
              <img
                src={recipient.userAvatar}
                alt=""
                className="be-ava-comment"
              />
              <h3 className="be-comment-name">{recipient.userName}</h3>
              <div className="x">
                <XLg onClick={closeMessageBar} />
              </div>
            </div>
          )}
        </div>
      </div>
      <div className="message-box-inner-container">
        {messages &&
          messages.length > 0 &&
          messages.map((message, key) => (
            <MessageBox
              key={key}
              text={message.text}
              timeStamp={message.timeStamp}
              sentByYou={message.sentByYou}
            ></MessageBox>
          ))}
        <div ref={messagesEndRef} />
      </div>
      {userData && recipient && (
        <div className="input-container">
          <input
            type="text"
            value={text}
            onChange={handleInput()}
            onKeyPress={createMessage()}
          />
        </div>
      )}
    </div>
  );
}

export default connect(
  (state) => ({
      showMes: showMessageSelecter(state),
      userData: userDataSelector(state)
  }),
  (dispatch) =>
      bindActionCreators(
          {
              ...modalActions, ...userActions
          },
          dispatch
      )
)(SingleMessage);
