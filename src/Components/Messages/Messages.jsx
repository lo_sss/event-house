import { useEffect, useState } from "react";

import {
  fromCorrespondencesDocument,
  getLiveMessages,
} from "../../Services/messages.Service";
import SingleMessage from "./SingleMessage/SingleMessage";
import "./Messages.css";

export default function Messages({ userName, recipientName }) {
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    const unsubscribe = getLiveMessages(
      (snapshot) => {
        setMessages(fromCorrespondencesDocument(snapshot));
      },
      userName,
      recipientName
    );

    return () => unsubscribe();
  }, [userName, recipientName]);

  return (
    <div className="message-windows-container">
      {recipientName && (
        <SingleMessage
          recipientName={recipientName}
          messages={messages.sort((x, y) => x.timeStamp - y.timeStamp)}
        />
      )}
    </div>
  );
}
