import React, { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { categories } from "../../Common/Constants";
import SlideShow from "../../Layouts/SlideShow/SlideShow";
import {
  fromEventsDocument,
  getLiveEvents,
} from "../../Services/events.service";
import { userSelector } from "../../reducers/user";
import userActions from "../../actions/user"
import './Home.css'

const Home = ({ user }) => {
  const [events, setEvents] = useState([]);

  useEffect(() => {
    const unsubscribe = getLiveEvents((snapshot) => {
      setEvents(fromEventsDocument(snapshot));
    });

    return () => unsubscribe();
  }, []);

  const filterEvents = (category) =>
    events.filter((event) => event.category === category);

  return (
    <div>
      {user && events.length > 0 ? (
        categories.map((category, key) => {
          const filteredEvents = filterEvents(category);
          if (filteredEvents.length === 0) return <div key={key}></div>;
          return (
            <div key={`${key}-${category}`}>
              <Container>
                <Row>
                  <Col>
                    <div className="emptySpace"></div>
                  </Col>
                  <Col />
                </Row>
                <Row>
                  <Col></Col>
                  <Col xs={12}>
                    <div className="styleWrapperCategory">
                      <p className="fw-bold category">
                        {category}
                      </p>
                    </div>
                  </Col>
                </Row>
              </Container>
              <SlideShow events={filteredEvents} itemsCount={3} />
            </div>
          );
        })
      ) : (
        <div className="empty"></div>
      )}
    </div>
  );
}

export default connect(
  (state) => ({
      user: userSelector(state)
  }),
  (dispatch) =>
      bindActionCreators(
          {
              ...userActions
          },
          dispatch
      )
)(Home);
