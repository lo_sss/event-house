import { Card, Container } from "react-bootstrap";
import "./AboutUs.css";

export default function AboutUs() {
  return (
    <Container>
      <Card className="searchCard">
        <Card.Body className="searchBody">
          <h3 className="textStyle">About us</h3>
          <div className="textStyle">Welcome to Event House</div>
          <div className="textStyle">
            Our main focus is the live scene, regardless of the genre. Our team
            is working hard to always provide the most up-to-date information
            about the current, past and future events in Bulgaria.
          </div>
          <div className="textStyle">
            Even so, we cannot do it without your help - our members!
          </div>
        </Card.Body>
      </Card>
    </Container>
  );
}
