import { useState } from "react";
import { Card, Form, Button, Alert, Container } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";

import { registerUser } from "../../Services/auth.Service";
import {
  additionalUserInfo,
  getUserByUsername,
} from "../../Services/users.Service";

export default function SignUp() {
  const [user, setUser] = useState({
    email: "",
    password: "",
    confirmPassword: "",
    userName: "",
    phoneNumber: "",
  });

  const [error, setError] = useState("");
  const navigate = useNavigate();

  const updateForm = (prop) => (e) => {
    setUser({
      ...user,
      [prop]: e.target.value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (user.userName.length < 2 || user.userName.length > 20) {
      return setError("User name must be between 2 and 20 symbols");
    }

    if (user.password !== user.confirmPassword) {
      return setError("Password does not match");
    }

    getUserByUsername(user.userName).then((snapshot) => {
      if (snapshot.exists()) {
        return setError(`${user.userName} is already taken`);
      }
      return registerUser(user.email, user.password)
        .then((u) => {
          return additionalUserInfo(
            user.userName,
            u.user.uid,
            u.user.email,
            user.phoneNumber
          );
        })
        .catch((err) => setError(`${err.message}`));
    });
    navigate("../home", { replace: true });
  };
  return (
    <>
      {error && <Alert variant="danger">{error}</Alert>}
      <Container
        className="d-flex align-items-center justify-content-center container-login">
        <div className="w-100 content-width">
          <Card>
            <Card.Body>
              <h2 className="text-center mb-4">Sign up</h2>
              {error && <Alert variant="danger">{error}</Alert>}
              <Form onSubmit={handleSubmit}>
                <Form.Group id="username">
                  <Form.Label>Username</Form.Label>
                  <Form.Control
                    type="text"
                    value={user.userName}
                    onChange={updateForm("userName")}
                    required
                  />
                </Form.Group>
                <Form.Group id="email">
                  <Form.Label>Email</Form.Label>
                  <Form.Control
                    type="email"
                    value={user.email}
                    onChange={updateForm("email")}
                    required
                  />
                </Form.Group>
                <Form.Group id="password">
                  <Form.Label className="mt-2">Password</Form.Label>
                  <Form.Control
                    type="password"
                    value={user.password}
                    onChange={updateForm("password")}
                    required
                  />
                </Form.Group>
                <Form.Group id="confirm-password">
                  <Form.Label className="mt-2">Confirm Password</Form.Label>
                  <Form.Control
                    type="password"
                    value={user.confirmPassword}
                    onChange={updateForm("confirmPassword")}
                    required
                  />
                </Form.Group>
                <Button
                  className="w-100 mt-4"
                  style={{backgroundColor: "#696969"}}
                  type="submit">
                  Sign Up
                </Button>
              </Form>
            </Card.Body>
          </Card>
          <div className="w-100 text-center mt-2 link-style">
            Already have an account ?{" "}
            <Link to="/login" className="link-style">
              Log in
            </Link>
          </div>
        </div>
      </Container>
    </>
  );
}
