import { Card, Container } from "react-bootstrap";

export default function PrivacyPolocy() {
  return (
    <Container>
      <Card className="searchCard">
        <Card.Body className="searchBody">
          <h3 className="textStyle">Privacy policy</h3>
          <h4 className="textStyle">Consent</h4>
          <div className="textStyle">
            Before disclosing any data, check if the proper consent is in place
            to do so. Depending on the type of data, you may need the consent of
            the individual concerned before passing it on.
          </div>
          <h4 className="textStyle">Purpose</h4>
          <div className="textStyle">
            Before collecting any data from an individual, make sure you need
            it. You should only collect the exact amount of data needed and
            never more than is required for the purpose. Also, data should not
            be collected or used without approval. Don`t collect more than is
            needed.
          </div>
          <h4 className="textStyle">Security and access</h4>
          <div className="textStyle">
            Data should always be kept and stored anonymously using the latest
            security and de-anonymization methods. You have a responsibility to
            protect data from loss, theft, unauthorized use and modification.
            Data should not be accessed without permission or a specific, lawful
            purpose. An individual should have the means to view and correct the
            data held on them as provided by law.
          </div>
          <h4 className="textStyle">Disclosure and accountability</h4>
          <div className="textStyle">
            Individuals will have a right to know why their data is being
            collected and how it will be used by law. Individuals can normally
            hold companies to account for the use of their data and keeping it
            safe and secure. Individuals may be able to sue or report companies
            who abuse or misuse data. Some of the fines can reach up to 4% of
            global annual turnover.
          </div>
          <h4 className="textStyle">Destruction and disposal</h4>
          <div className="textStyle">
            Data should not be kept for any longer than is necessary. Data that
            is not being used, out of use, or no longer required should be
            destroyed. All data should have a retention period associated with
            it, and data should be periodically reassessed and destroyed or
            disposed of when no longer needed.
          </div>
        </Card.Body>
      </Card>
    </Container>
  );
}
