import { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useLoadScript } from "@react-google-maps/api";
import {
  BookmarkFill,
  BoomboxFill,
  FlagFill,
  GeoAltFill,
  GeoFill,
  TicketPerforatedFill,
} from "react-bootstrap-icons";
import { Card, Col, Container, Row } from "react-bootstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import {
  findEventAddress,
  followEvent,
  getEventById,
} from "../../../Services/events.service";
import followButton from "../../../Assets/follow.png";
import Map from "../../../Common/Map/Map";
import { fromPostsDocument, getLivePosts } from "../../../Services/posts.Service";
import Post from "../../../Common/Post/Post";
import { userDataSelector } from "../../../reducers/user";
import userActions from '../../../actions/user'
import "./SingleEvent.css";

const SingleEvent = ({ userData }) => {
  const { id } = useParams();
  const navigate = useNavigate();
  const [event, setEvent] = useState(null);
  const [location, setLocation] = useState(null);
  const [posts, setPosts] = useState([]);
  const { isLoaded } = useLoadScript({
    googleMapsApiKey:
      //  process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY
      "AIzaSyBt4zfkpWpafR3PqMGCL7LHoXfB5L8EFc4",
  });

  useEffect(() => {
    getEventById(id)
      .then((result) => {
        setEvent(result);
        return result;
      })
      .then((event) => {
        findEventAddress(event.area, event.address).then((results) => {
          const location = results.results[0].geometry.location;
          setLocation(location);
        });
      });

    const unsubscribe = getLivePosts((snapshot) => {
      setPosts(filterPosts(id, fromPostsDocument(snapshot)));
    });

    return () => unsubscribe();
  }, [id]);

  const filterPosts = (id, result) => {
    return result.filter((post) => post.event === id);
  };

  const follow = () => {
    followEvent(userData.userName, event.uid).then(() => {
      event.followedBy.push(userData.userName);
      setEvent({ ...event, followedBy: event.followedBy });
    });
  };

  return (
    <>
      {event && userData ? (
        <>
          <div className="main">
            <div className="date">
              <h2>{new Date(event.date).toString().slice(0, 3)}</h2>
              <h2 id="date">{new Date(event.date).toString().slice(8, 10)}</h2>
              <h2 id="month">{new Date(event.date).toString().slice(4, 7)}</h2>
              <h2 id="year">{new Date(event.date).toString().slice(11, 15)}</h2>
            </div>

            <div className="time">
              <h2>{event.time.split(":")[0]}</h2>
              <h3>:</h3>
              <h2 id="date">{event.time.split(":")[1]}</h2>
            </div>
          </div>

          <div>
            <ol className="banner-style">
              <img
                src={event.bannerUrl}
                alt="banner"
                width="70%"
                height="400"
                className="containerImg"
              />
            </ol>
            {!event.followedBy.includes(userData.userName) && (
              <img
                src={followButton}
                alt="banner"
                className="img2"
                onClick={follow}
              />
            )}
            <div className="cardContain fw-bold followers">
              {event.followedBy.length} FOLLOWERS
            </div>
          </div>

          <p
            className="fw-bold event-title">
            {event.title}
          </p>

          <Container>
            <Card className="description-card">
              <Card.Body className="textStyle">
                <Container>
                  <Row>
                    <Col>
                      <BookmarkFill className="icon-style" />
                      <span className="cardContain fw-bold">
                        {event.category}
                      </span>
                    </Col>
                    <Col>
                      <GeoFill className="icon-style" />
                      <span className="cardContain fw-bold">{event.area}</span>
                    </Col>
                  </Row>
                  <Row className="content-style">
                    <Col>
                      <FlagFill className="icon-style" />
                      <span className="cardContain fw-bold">
                        Event created by{" "}
                        <span
                          className="event-author"
                          onClick={() => navigate(`/profile/${event.author}`)}
                        >
                          {event.author}
                        </span>
                      </span>
                    </Col>
                    <Col>
                      <GeoAltFill className="icon-style" />
                      <span className="cardContain fw-bold">
                        {event.address}
                      </span>
                    </Col>
                  </Row>
                  <Row className="content-style">
                    <Col>
                      <BoomboxFill className="icon-style" />
                      <span className="cardContain fw-bold">
                        {event.performers}
                      </span>
                    </Col>
                    <Col>
                      {!isLoaded ? (
                        <div>Loading...</div>
                      ) : (
                        <>
                          {!location ? (
                            <div>Loading map...</div>
                          ) : (
                            <Map location={location} />
                          )}
                        </>
                      )}
                    </Col>
                  </Row>
                  <Row className="content-style">
                    <Col>
                      <TicketPerforatedFill className="icon-style" />
                      {event.tickets.startsWith("www") ||
                      event.tickets.startsWith("http") ? (
                        <a className="cardContain fw-bold" href={event.tickets}>
                          {event.tickets}
                        </a>
                      ) : (
                        <span className="cardContain fw-bold">
                          {event.tickets}
                        </span>
                      )}
                    </Col>
                  </Row>
                  <Row className="content-style">
                    <Col>
                      <span className="descriptionContain">
                        {event.description}
                      </span>
                    </Col>
                  </Row>
                </Container>
              </Card.Body>
            </Card>

            <Row>
              <Col xs="1" md="1"></Col>
              <Col xs="1" md="9">
                <div className="content-style">
                  {posts &&
                    posts.map((post, key) => (
                      <Post
                        key={key}
                        post={post}
                        userAvatar={post.authorAvatar}
                      />
                    ))}
                </div>
              </Col>
            </Row>
          </Container>
        </>
      ) : (
        <div className="empty"></div>
      )}
    </>
  );
}

export default connect(
  (state) => ({
      userData: userDataSelector(state)
  }),
  (dispatch) =>
      bindActionCreators(
          {
              ...userActions
          },
          dispatch
      )
)(SingleEvent);
