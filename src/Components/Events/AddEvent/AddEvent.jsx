import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Button,
  Modal,
  Form,
  Container,
  Row,
  Col,
  Alert,
} from "react-bootstrap";
import TextField from "@mui/material/TextField";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { LocalizationProvider } from "@mui/x-date-pickers/LocalizationProvider";
import InputAdornment from "@mui/material/InputAdornment";
import { DateTimePicker } from "@mui/x-date-pickers/DateTimePicker";
import Autocomplete from "@mui/material/Autocomplete";
import {
  ref as storageRef,
  uploadBytes,
  getDownloadURL,
} from "firebase/storage";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import modalActions from '../../../actions/modal'
import { addEvent, updateEventUrl } from "../../../Services/events.service";
import { storage } from "../../../Config/Firebase-config";
import { areas, categories } from "../../../Common/Constants";
import { showAddEventSelecter } from "../../../reducers/modal";
import { userDataSelector } from "../../../reducers/user";
import userActions from '../../../actions/user'
import "./AddEvent.css"

const AddEvent = ({ showEvent, hideAddEvent, userData }) => {
  const [eventContent, setEventContent] = useState({});
  const [filePath, setFilePath] = useState("");
  const [error, setError] = useState(null);
  const navigate = useNavigate();

  const throwError = (message) => {
    throw Error(message);
  };

  const handleClose = () => hideAddEvent();

  const updateEvent = (prop) => (e) => {
    e.preventDefault();
    setEventContent({
      ...eventContent,
      [prop]: e.target.value,
      author: userData.userName,
    });
  };

  const updatePick = (prop, value) => {
    setEventContent({
      ...eventContent,
      [prop]: value,
    })
  }

  const updateDate = (value) => {
    setEventContent({
      ...eventContent,
      date: value.valueOf(),
      time: value.toString().slice(16, -43),
    })
  }

  const updateUploadFile = () => (e) => {
    e.preventDefault();
    setFilePath(e.target?.files[0]);
  };

  const createEvent = (content) => async (e) => {
    try {
      validateEventContent(content);
      if (!filePath) {
        const message = "Please add event banner";
        setError({ status: true, message });
        throwError(message);
      }
      e.preventDefault();
      hideAddEvent();
      const event = await addEvent(content);
      uploadEventBanner(event.uid);
      navigate(`/`);
      return event;
    } catch (e) {
      console.error(e);
    }
  };

  const validations = {
    title: 'Please add title',
    category: 'Please pick category',
    area: 'Please pick area',
    address: 'Please add address',
    performers: 'Please add performers',
    description: 'Please add description',
    tickets: 'Please add tickets'
  }

  const validateEventContent = (content) => {

    if (!content.date || typeof content.date.valueOf() !== "number") {
      const message = "Please pick date and time from calendar";
      setError({ status: true, message });
      throwError(message);
    }

    Object.entries(validations).forEach(([key, value]) => {
       if(!content[key]) {
        setError({ status: true, message: value });
        throwError(value);
       }
    })
  };

  const uploadEventBanner = (uid) => {
    const file = filePath;

    const picture = storageRef(storage, `events/images/${uid}`);

    uploadBytes(picture, file)
      .then((snapshot) => {
        return getDownloadURL(snapshot.ref).then((url) => {
          return updateEventUrl(uid, url);
        });
      })
      .catch(console.error);
  };

  const areaProps = {
    options: areas,
    getOptionLabel: (option) => option,
  };

  const categoryProps = {
    options: categories,
    getOptionLabel: (option) => option,
  };

  return (
    <Container className="containerHeight">
      <Modal size="lg" show={showEvent} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>New event</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group className="mb-3">
              <TextField
                label="Title"
                id="outlined-start-adornment"
                sx={{ m: 1, width: "98%" }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start"></InputAdornment>
                  ),
                }}
                onChange={updateEvent("title")}
              />
            </Form.Group>

            <Container>
              <Row>
                <Col>
                  <Autocomplete
                    {...categoryProps}
                    id="categories"
                    onChange={(event, value) => {
                      updatePick('category', value)
                    }}
                    renderInput={(params) => (
                      <TextField {...params} label="category" />
                    )}
                  />
                </Col>
                <Col>
                  <Autocomplete
                    {...areaProps}
                    id="areas"
                    onChange={(event, value) => {
                      updatePick('area', value)
                    }}
                    renderInput={(params) => (
                      <TextField {...params} label="area" />
                    )}
                  />
                </Col>
                <Col xs={4}>
                  <Form.Group className="mb-3">
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                      <DateTimePicker
                        renderInput={(props) => <TextField {...props} />}
                        minDate={new Date()}
                        label="Date and time"
                        onChange={(newValue) => {
                          updateDate(newValue);
                        }}
                      />
                    </LocalizationProvider>
                  </Form.Group>
                </Col>
              </Row>
            </Container>

            <Form.Group className="mb-3">
              <TextField
                label="Address"
                id="outlined-start-adornment"
                sx={{ m: 1, width: "98%" }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start"></InputAdornment>
                  ),
                }}
                onChange={updateEvent("address")}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <TextField
                label="Performers"
                id="outlined-start-adornment"
                sx={{ m: 1, width: "98%" }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start"></InputAdornment>
                  ),
                }}
                onChange={updateEvent("performers")}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <TextField
                label="Description"
                id="outlined-start-adornment"
                sx={{ m: 1, width: "98%" }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start"></InputAdornment>
                  ),
                }}
                onChange={updateEvent("description")}
              />
            </Form.Group>

            <Form.Group className="mb-3">
              <TextField
                label="Link to buy tickets"
                id="outlined-start-adornment"
                sx={{ m: 1, width: "98%" }}
                InputProps={{
                  startAdornment: (
                    <InputAdornment position="start"></InputAdornment>
                  ),
                }}
                onChange={updateEvent("tickets")}
              />
            </Form.Group>

            <div className="mb-3">
              <label htmlFor="formFile" className="form-label">
                Default file input example
              </label>
              <input
                className="form-control"
                type="file"
                id="formFile"
                onChange={updateUploadFile()}
              />
            </div>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          {error && (
            <Alert
              variant="danger"
              onClose={() => hideAddEvent()}
              dismissible
              className="errorStyle"
            >
              <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
              <p>{error.message}</p>
            </Alert>
          )}

          <Button variant="secondary" onClick={createEvent(eventContent)}>
            Add event
          </Button>
        </Modal.Footer>
      </Modal>
    </Container>
  );
}

export default connect(
  (state) => ({
      showEvent: showAddEventSelecter(state),
      userData: userDataSelector(state)
  }),
  (dispatch) =>
      bindActionCreators(
          {
              ...modalActions, ...userActions
          },
          dispatch
      )
)(AddEvent);
