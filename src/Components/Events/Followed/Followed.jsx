import { useEffect, useState } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { categories } from "../../../Common/Constants";
import SlideShow from "../../../Layouts/SlideShow/SlideShow";
import { userDataSelector } from "../../../reducers/user";
import {
  fromEventsDocument,
  getLiveEvents,
} from "../../../Services/events.service";
import userActions from '../../../actions/user'
import "./Followed.css"

const Followed = ({ userData }) => {
  const [events, setEvents] = useState([]);

  useEffect(() => {
    const unsubscribe = getLiveEvents((snapshot) => {
      setEvents(fromEventsDocument(snapshot));
    });

    return () => unsubscribe();
  }, []);

  const filterEvents = (category) => {
    return events.filter(
      (event) =>
        event.category === category &&
        event.followedBy.includes(userData.userName)
    );
  };

  return (
    <div className="containerHeight">
      {userData && events.length > 0 ? (
        categories.map((category, key) => {
          const filteredEvents = filterEvents(category);
          if (filteredEvents.length === 0) return <div key={key}></div>;
          return (
            <div key={key}>
              <Container>
                <Row>
                  <Col>
                    <div className="emptySpace"></div>
                  </Col>
                  <Col></Col>
                </Row>
                <Row>
                  <Col></Col>
                  <Col xs={12}>
                    <div className="styleWrapperCategory">
                      <p className="fw-bold category">
                        {category}
                      </p>
                    </div>
                  </Col>
                </Row>
              </Container>
              <SlideShow events={filteredEvents} itemsCount={3} />
            </div>
          );
        })
      ) : (
        <div className="empty"></div>
      )}
    </div>
  );
}

export default connect(
  (state) => ({
      userData: userDataSelector(state)
  }),
  (dispatch) =>
      bindActionCreators(
          {
              ...userActions
          },
          dispatch
      )
)(Followed);
