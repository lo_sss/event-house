import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import Stack from "@mui/material/Stack";
import { useEffect, useState } from "react";
import { Card, Col, Container, Row } from "react-bootstrap";
import { DateTimePicker, LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import {
  fromEventsDocument,
  getLiveEvents,
} from "../../../Services/events.service";
import { areas, categories } from "../../../Common/Constants";
import { compareDates } from "../../../Common/helper";
import SlideShow from "../../../Layouts/SlideShow/SlideShow";
import { userSelector } from "../../../reducers/user";
import userActions from '../../../actions/user'
import "./Search.css";

const Search = ({ user }) => {
  const [events, setEvents] = useState([]);
  const [date, setDate] = useState(null);
  const [searchValue, setSearchValue] = useState(null);
  const [area, setArea] = useState(null);
  const [category, setCategory] = useState(null);
  const [results, setResults] = useState([]);

  useEffect(() => {
    const unsubscribe = getLiveEvents((snapshot) => {
      const events = fromEventsDocument(snapshot).map((event) => {
        return { ...event, searchOptions: setSearchOptions(event) };
      });

      setEvents(events);
    });

    return () => unsubscribe();
  }, []);

  const setSearchOptions = (event) => {
    return `${event.title}, ${event.performers}, ${event.address} by ${event.author}`;
  };

  const searchProps = {
    options: events,
    getOptionLabel: (option) => option.searchOptions,
  };

  const areaProps = {
    options: areas,
    getOptionLabel: (option) => option,
  };

  const categoryProps = {
    options: categories,
    getOptionLabel: (option) => option,
  };

  const handleResults = () => {
    setResults([]);

    if (searchValue) {
      setResults([searchValue]);
    } else {
      setResults(filterSearch(date, area, category));
    }
  };

  const filterSearch = (date, area, category) => {
    let sumResults = [];

    if (date && area && category) {
      sumResults = events.filter(
        (event) =>
          compareDates(event.date, date.valueOf()) &&
          event.area === area &&
          event.category === category
      );
    } else if (date && area) {
      sumResults = events.filter(
        (event) =>
          compareDates(event.date, date.valueOf()) && event.area === area
      );
    } else if (date && category) {
      sumResults = events.filter(
        (event) =>
          compareDates(event.date, date.valueOf()) &&
          event.category === category
      );
    } else if (area && category) {
      sumResults = events.filter(
        (event) => event.area === area && event.category === category
      );
    } else if (date) {
      sumResults = events.filter((event) =>
        compareDates(event.date, date.valueOf())
      );
    } else if (area) {
      sumResults = events.filter((event) => event.area === area);
    } else if (category) {
      sumResults = events.filter((event) => event.category === category);
    }

    return sumResults;
  };

  return (
    <Container>
      {user ? (
        <Card className="searchCard">
          <Card.Body className="searchBody">
            <Stack>
              <Container>
                <Row>
                  <Col>
                    <Autocomplete
                      {...searchProps}
                      id="events"
                      renderOption={(props, option) => (
                        <li {...props} key={option.uid}>
                          {option.title}
                        </li>
                      )}
                      onChange={(event, value) => {
                        setSearchValue(value);
                      }}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="Search"
                          variant="standard"
                        />
                      )}
                    />
                  </Col>
                </Row>
                <Row className="props-style">
                  <Col>
                    <Autocomplete
                      {...areaProps}
                      id="areas"
                      onChange={(event, value) => {
                        setArea(value);
                      }}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="area"
                          variant="standard"
                        />
                      )}
                    />
                  </Col>

                  <Col>
                    <Autocomplete
                      {...categoryProps}
                      id="categories"
                      onChange={(event, value) => {
                        setCategory(value);
                      }}
                      renderInput={(params) => (
                        <TextField
                          {...params}
                          label="category"
                          variant="standard"
                        />
                      )}
                    />
                  </Col>

                  <Col className="d-flex justify-content-end">
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                      <DateTimePicker
                        renderInput={(props) => <TextField {...props} />}
                        label="Date and time"
                        value={date}
                        onChange={(newValue) => {
                          setDate(newValue);
                        }}
                      />
                    </LocalizationProvider>
                  </Col>
                </Row>
              </Container>
              <div className="col-md-12 text-right">
                <button
                  type="button"
                  className="btn btn-secondary btn-lg buttonLocate"
                  onClick={handleResults}
                >
                  Search
                </button>
              </div>
              <h3 className="textStyle">Matches</h3>
            </Stack>
            {results && results.length > 0 ? (
              <SlideShow events={results} itemsCount={2} />
            ) : (
              <div className="styleContent">No matches</div>
            )}
          </Card.Body>
        </Card>
      ) : (
        <div className="empty"></div>
      )}
    </Container>
  );
}

export default connect(
  (state) => ({
      user: userSelector(state)
  }),
  (dispatch) =>
      bindActionCreators(
          {
              ...userActions
          },
          dispatch
      )
)(Search);
