import { GoogleMap, Marker } from "@react-google-maps/api";
import "./Map.css";

export default function Map(location) {
  return (
    <GoogleMap
      zoom={13}
      center={{ lat: location.location.lat, lng: location.location.lng }}
      mapContainerClassName="map-container"
    >
      <Marker
        position={{ lat: location.location.lat, lng: location.location.lng }}
      />
    </GoogleMap>
  );
}
