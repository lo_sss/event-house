import { useEffect, useState } from "react";
import { Accordion, Container } from "react-bootstrap";
import { HandThumbsUp, HandThumbsUpFill, Trash } from "react-bootstrap-icons";
import { useNavigate } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import PopModal from "../../Layouts/Modals/PopModal/PopModal";
import { getEventById } from "../../Services/events.service";
import {
  deletePost,
  dislikePost,
  likePost,
} from "../../Services/posts.Service";
import CommentInput from "../Comments/CommentInput/CommentInput";
import Comments from "../Comments/Comments";
import { defineDateAndTime } from "../helper";
import PostMedia from "../PostMedia/PostMedia";
import { userDataSelector } from "../../reducers/user";
import userActions from '../../actions/user'
import "./Post.css";

const Post = ({ post, userAvatar, userData }) => {
  const [show, setShow] = useState(false);
  const [event, setEvent] = useState(null);
  const navigate = useNavigate();

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    getEventById(post.event).then((result) => {
      setEvent(result);
    });
  }, [post.event]);

  const createdOn = defineDateAndTime(new Date(post.createdOn));

  const delPost = () => {
    deletePost(post.uid, post.likedBy).then(() => {
      handleClose();
    });
  };

  const like = () => {
    likePost(userData.userName, post.uid).then(() => {
      post.likedBy.push(userData.userName);
    });
  };

  const dislike = () => {
    dislikePost(userData.userName, post.uid).then(() => {});
  };

  return (
    <>
      {
        <Container className="container posts d-flex h-100 w-100">
          {post ? (
            <ul className="timeline">
              <li>
                <div id={createdOn.id} className="timeline-time">
                  <span className="date">{createdOn.day}</span>
                  <span className="time">{createdOn.time}</span>
                </div>
                <div className="timeline-icon">
                  <span>&nbsp;</span>
                </div>
                <div className="timeline-body">
                  <div className="timeline-header">
                    <div className="user-info">
                      <div className="userimage">
                        <img src={userAvatar} alt="profilepic" />
                      </div>
                      <span
                        className="userName"
                        onClick={() => navigate(`/profile/${post.author}`)}
                      >
                        {post.author}
                      </span>
                    </div>
                    <div className="timeline-post-actions">
                      {userData && post.author === userData.userName && (
                        <>
                          <Trash
                            onClick={(e) => handleShow(e)}
                            className="dark timeline-post-actions-icon delete"
                          />
                          <PopModal
                            show={show}
                            onConfirm={delPost}
                            handleClose={handleClose}
                            bodyText={
                              "Are you sure you want to delete the current post?"
                            }
                          />
                        </>
                      )}
                    </div>
                  </div>
                  <div className="timeline-content">
                    <PostMedia images={post.images} videos={post.videos} />
                    <p className="timeline-content-text">{post.title}</p>
                  </div>
                  <div className="timeline-footer">
                    <div className="timeline-likes-container">
                      <div className="like-icon">
                        {userData &&
                        post.likedBy.includes(userData.userName) ? (
                          <HandThumbsUpFill
                            onClick={dislike}
                            className="like-liked"
                          />
                        ) : (
                          userData && (
                            <HandThumbsUp
                              onClick={like}
                              className="like-not-liked"
                            />
                          )
                        )}
                      </div>
                      <div className="timeline-likes">
                        <div className="stats">
                          <span className="stats-total">
                            {post.likedBy.length} LIKES
                          </span>
                        </div>
                      </div>
                    </div>
                    <div>
                      {event && (
                        <div className="eventBanner">
                          <img
                            onClick={() => navigate(`/events/${event.uid}`)}
                            src={event.bannerUrl}
                            alt="event"
                            width="100"
                            height="50"
                          />
                        </div>
                      )}
                    </div>
                  </div>

                  {post.metrics.comments > 0 ? (
                    <Comments post={post} userAvatar={userAvatar} />
                  ) : (
                    <Accordion
                      className="show-hide-comments"
                      defaultActiveKey={1}
                    ></Accordion>
                  )}
                  {userData && <CommentInput post={post} />}
                </div>
              </li>
            </ul>
          ) : null}
        </Container>
      }
    </>
  );
}

export default connect(
  (state) => ({
      userData: userDataSelector(state)
  }),
  (dispatch) =>
      bindActionCreators(
          {
              ...userActions
          },
          dispatch
      )
)(Post);
