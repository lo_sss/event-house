import React from "react";
import { Col, Container, Row } from "react-bootstrap";
import "./PostMedia.css"

export default function PostMedia({ images, videos }) {
  const getUrls = (files) => {
    return Object.values(files);
  };

  return (
    <Container className="d-flex p-0 media-container w-100 h-100 my-3">
      <Row>
        {images &&
          getUrls(images).map((image, key) => (
            <Col key={key}>
              <img
                className="img-fluid imgStyle"
                src={image}
                alt=""
              />
            </Col>
          ))}

        {videos &&
          getUrls(videos).map((video, key) => (
            <Col key={key}>
              <video
                className="img-fluid imgStyle"
                controls
              >
                <source src={video} type="video/mp4" />
              </video>
            </Col>
          ))}
      </Row>
    </Container>
  );
}
