export const defineDateAndTime = (inputDate) => {
  const myDate = inputDate.toLocaleDateString("uk-Uk", {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
    hour: "2-digit",
    minute: "2-digit",
  });

  const getDay = (input) => {
    const today = new Date();
    if (
      inputDate.getMonth() === today.getMonth() &&
      inputDate.getFullYear() === today.getFullYear()
    ) {
      switch (today.getDate() - input.getDate()) {
        case 0:
          return "today";
        case 1:
          return "yesterday";
        case 2:
          return "2 days ago";
        case 3:
        case 4:
        case 5:
        case 6:
          return "few days ago";
        case 7:
          return "a week ago";
        case 8:
        case 9:
        case 10:
        case 11:
        case 12:
        case 13:
        case 14:
          return "more than week ago";
        default:
          break;
      }
    }
  };

  return {
    id: myDate.slice(0, 10),
    day: getDay(inputDate),
    time: `${myDate.slice(11, 14)}:${myDate.slice(15, 18)}`,
  };
};

export const commentDateFormat = (date) => {
  const month = date.toLocaleString("default", {
    month: "long",
  });
  const dayDate = date.getDate();
  const year = date.getFullYear();
  const hours = date.getHours();
  const minutes = date.getMinutes();

  return `${month} ${dayDate}, ${year} at ${hours}:${minutes}`;
};

export const compareDates = (date1, date2) => {
  const date1Obj = {
    year: new Date(date1).toString().slice(11, 15),
    month: new Date(date1).toString().slice(4, 7),
    date: new Date(date1).toString().slice(8, 10),
  };

  const date2Obj = {
    year: new Date(date2).toString().slice(11, 15),
    month: new Date(date2).toString().slice(4, 7),
    date: new Date(date2).toString().slice(8, 10),
  };

  if (
    date1Obj.year === date2Obj.year &&
    date1Obj.month === date2Obj.month &&
    date1Obj.date === date2Obj.date
  )
    return true;

  return false;
};
