import { useEffect, useState } from "react";
import { Trash } from "react-bootstrap-icons";
import { connect } from "react-redux";
import { useNavigate } from "react-router-dom";
import { bindActionCreators } from "redux";

import { userDataSelector } from "../../../reducers/user";
import {
  deleteComment,
  getCommentById,
} from "../../../Services/comments.Service";
import { getUserByUsername } from "../../../Services/users.Service";
import { commentDateFormat } from "../../helper";
import userActions from '../../../actions/user'
import "./CommentBox.css";

const CommentBox = ({ commentId, postId, userData }) => {
  const [comment, setComment] = useState(null);
  const [user, setUser] = useState(null);
  const navigate = useNavigate();

  useEffect(() => {
    getCommentById(commentId)
      .then((result) => {
        setComment({
          id: commentId,
          author: result.author,
          content: result.content,
          createdOn: result.createdOn,
          authorAvatar: result.authorAvatar,
        });
        getUserByUsername(result.author).then((snapshot) =>
          setUser(snapshot.val())
        );
        return result;
      })
      .catch((e) => {
        console.error(e);
      });
  }, [commentId]);

  return (
    <>
      {comment && user && (
        <div className="be-comment-block">
          <div className="be-comment">
            <div className="be-comment-header">
              <div className="be-comment-author">
                {userData && comment.author === userData.userName ? (
                  <img
                    src={userData.userAvatar}
                    alt=""
                    className="be-ava-comment"
                  />
                ) : (
                  <img
                    src={user.userAvatar}
                    alt=""
                    className="be-ava-comment"
                  />
                )}
                <h3
                  className="be-comment-name"
                  onClick={() => navigate(`/profile/${comment.author}`)}
                >
                  {comment.author}
                </h3>
              </div>
              <div className="be-comment-time">
                <i className="fa fa-clock-o"></i>
                {commentDateFormat(comment.createdOn)}
                {userData && comment.author === userData.userName && (
                  <>
                    <Trash
                      className="icon delete-comment"
                      onClick={() => {
                        deleteComment(commentId, postId);
                      }}
                    />
                  </>
                )}
              </div>
            </div>
            <p className="be-comment-text">{comment.content}</p>
          </div>
        </div>
      )}
    </>
  );
}

export default connect(
  (state) => ({
      userData: userDataSelector(state)
  }),
  (dispatch) =>
      bindActionCreators(
          {
              ...userActions
          },
          dispatch
      )
)(CommentBox);
