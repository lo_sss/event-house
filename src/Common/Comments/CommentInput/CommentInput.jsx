import { useState } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import { userDataSelector } from "../../../reducers/user";
import { addComment } from "../../../Services/comments.Service";
import userActions from '../../../actions/user'
import "./CommentInput.css";

const CommentInput = ({ post, userData }) => {
  const [comment, updateComment] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    return addComment(post.uid, userData.userName, comment, userData.userAvatar).then(() => {
      updateComment("");
    });
  };

  return (
    <div className="timeline-comment-box">
      <div className="user">
        <img src={userData.userAvatar} alt="avatar" />
      </div>
      <div className="input">
        <form onSubmit={handleSubmit}>
          <div className="input-group">
            <input
              onChange={(e) => updateComment(e.target.value)}
              value={comment}
              type="text"
              className="form-control rounded-corner"
              placeholder="Write a comment..."
              required
            />
            <span className="input-group-btn p-l-10">
              <button
                variant="btn-outline-success"
                className="btn btn-outline-success f-s-12 rounded-corner"
                type="submit"
              >
                Comment
              </button>
            </span>
          </div>
        </form>
      </div>
    </div>
  );
}

export default connect(
  (state) => ({
      userData: userDataSelector(state)
  }),
  (dispatch) =>
      bindActionCreators(
          {
              ...userActions
          },
          dispatch
      )
)(CommentInput);
