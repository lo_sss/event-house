import { Accordion } from "react-bootstrap";
import { ChatDotsFill } from "react-bootstrap-icons";
import CommentBox from "./CommentBox/CommentBox";

export default function Comments({ post, userAvatar }) {
    return (
      <Accordion defaultActiveKey={0}>
        <Accordion.Item eventKey="0">
          <Accordion.Header>
            <div className="timeline-likes">
              <span className="stats-text">
                <ChatDotsFill className="comments" /> Comments{' '}
                {`(${post.comments.length})`}
              </span>
            </div>
          </Accordion.Header>
          <Accordion.Body>
            {post.comments.map((commentId, key) => {
              return (
                <CommentBox
                  key={key}
                  className=" comments-container w-100 mt-5"
                  commentId={commentId}
                  postId={post.uid}
                  userAvatar={userAvatar}
                />
              );
            })}
          </Accordion.Body>
        </Accordion.Item>
      </Accordion>
    );
  }
  