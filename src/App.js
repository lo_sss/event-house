import React, { useEffect, useState } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import Login from "./Components/Login/Login";
import Home from "./Components/Home/Home";
import SignUp from "./Components/SignUp/SignUp";
import Navmenu from "./Layouts/Navbar/Navbar";
import Search from "./Components/Events/Search/Search";
import AddEvent from "./Components/Events/AddEvent/AddEvent";
import Followed from "./Components/Events/Followed/Followed";
import NewEvents from "./Components/Events/NewEvents/NewEvents";
import Profile from "./Components/Profile/Profile";
import Footer from "./Layouts/Footer/Footer";
import PrivacyPolicy from "./Components/PrivacyPolicy/PrivacyPolicy";
import AboutUs from "./Components/AboutUs/AboutUs";
import SingleEvent from "./Components/Events/SungleEvent/SignleEvent";
import PageNotFound from "./Components/PageNotFound/PageNotFound";
import { useAuthState } from "react-firebase-hooks/auth";
import { auth } from "./Config/Firebase-config";
import { getUserData } from "./Services/users.Service";
import { userDataSelector, userSelector } from "./reducers/user";
import userActions from "../src/actions/user";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";

const App = ({ setUser, setUserData }) => {
  const [error, setError] = useState("");
  const [user] = useAuthState(auth);

  useEffect(() => {
    if (user === null) return;

    getUserData(user.uid)
      .then((snapshot) => {
        if (!snapshot.exists) {
          return setError("User does not exist :(");
        }

        setUser({ user });
        setUserData({
          userData: snapshot.val()[Object.keys(snapshot.val())[0]],
        });
      })
      .catch((err) => setError(`${err.message}`));
  }, [user]);

  return (
    <Router>
      <div className="app-container">
        <Navmenu />
        <Routes>
          <Route index element={<Home />} />
          <Route path="signup" element={<SignUp />} />
          <Route path="login" element={<Login />} />
          <Route path="home" element={<Home />} />
          <Route path="search" element={<Search />} />
          <Route path="add-event" element={<AddEvent />} />
          <Route path="followed" element={<Followed />} />
          <Route path="new-events" element={<NewEvents />} />
          <Route path="events/:id" element={<SingleEvent />} />
          <Route path="profile/:userName" element={<Profile />} />
          <Route path="privacy-policy" element={<PrivacyPolicy />} />
          <Route path="about-us" element={<AboutUs />} />
          <Route path="*" element={<PageNotFound />} />
        </Routes>
        <Footer />
      </div>
    </Router>
  );
};

export default connect(
  (state) => ({
    user: userSelector(state),
    userData: userDataSelector,
  }),
  (dispatch) =>
    bindActionCreators(
      {
        ...userActions,
      },
      dispatch
    )
)(App);
