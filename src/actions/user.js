import { createActions } from './utils';

export const setUser = 'SET_USER';
export const setUserData = 'SET_USER_DATA';


export default createActions({
    setUser,
    setUserData,
});