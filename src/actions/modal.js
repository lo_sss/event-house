import { createActions } from './utils';

export const showAddEvent = 'SHOW_ADD_EVENT';
export const hideAddEvent = 'HIDE_ADD_EVENT';
export const showAvatarModal = 'SHOW_AVATAR_MODAL';
export const hideAvatarModal = 'HIDE_AVATAR_MODAL';
export const showMoodModal = 'SHOW_MOOD_MODAL';
export const hideMoodModal = 'HIDE_MOOD_MODAL';
export const showPostModal = 'SHOW_POST_MODAL';
export const hidePostModal = 'HIDE_POST_MODAL';
export const showMessageModal = 'SHOW_MESSAGE_MODAL';
export const hideMessageModal = 'HIDE_MESSAGE_MODAL';


export default createActions({
    showAddEvent,
    hideAddEvent,
    showAvatarModal,
    hideAvatarModal,
    showMoodModal,
    hideMoodModal,
    showPostModal,
    hidePostModal,
    showMessageModal,
    hideMessageModal,
});