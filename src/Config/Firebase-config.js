import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getDatabase } from "firebase/database";
import { getStorage } from "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyCny302WQE83aQTnlreW-oJ0q9RLFB1QKo",
  authDomain: "event-house-7ad86.firebaseapp.com",
  projectId: "event-house-7ad86",
  storageBucket: "event-house-7ad86.appspot.com",
  messagingSenderId: "339619533322",
  appId: "1:339619533322:web:c6cf08c9bb6b81441805f3",
  databaseURL:
    "https://event-house-7ad86-default-rtdb.europe-west1.firebasedatabase.app/",
};

export const app = initializeApp(firebaseConfig);

export const auth = getAuth(app);

export const db = getDatabase(app);

export const storage = getStorage(app);
