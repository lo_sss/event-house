import { useState } from "react";
import { Alert, Button, Form, Modal } from "react-bootstrap";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import modalActions from '../../../actions/modal'
import TextField from "@mui/material/TextField";
import InputAdornment from "@mui/material/InputAdornment";
import { updateUserMood } from "../../../Services/users.Service";
import { showMoodSelecter } from "../../../reducers/modal";
import { userDataSelector, userSelector } from "../../../reducers/user";
import userActions from '../../../actions/user'

const MoodModal = ({ showMood, hideMoodModal, user, userData, setUserData }) => {
  const [mood, setMood] = useState("");
  const [error, setError] = useState(null);

  const throwError = (message) => {
    throw Error(message);
  };

  const handleClose = () => hideMoodModal();

  const updateMood = () => (e) => {
    e.preventDefault();
    setMood(e.target.value);
  };

  const addMood = () => (e) => {
    try {
      if (!mood || mood.length > 100) {
        const message = "Please add mood up to 100 characters";
        setError({ status: true, message });
        throwError(message);
      }
      e.preventDefault();
      handleClose();
      setUserData({ userData: {...userData, mood: mood}})
      updateUserMood(userData.userName, mood);
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <Modal show={showMood} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Show your mood</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group className="mb-3">
            <TextField
              label="Mood"
              id="outlined-start-adornment"
              sx={{ m: 1, width: "98%" }}
              InputProps={{
                startAdornment: (
                  <InputAdornment position="start"></InputAdornment>
                ),
              }}
              onChange={updateMood()}
            />
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        {error && (
          <Alert
            variant="danger"
            onClose={() => hideMoodModal()}
            dismissible
            className="errorStyle"
          >
            <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
            <p>{error.message}</p>
          </Alert>
        )}
        {userData && (
          <Button variant="secondary" onClick={addMood()}>
            Save Changes
          </Button>
        )}
      </Modal.Footer>
    </Modal>
  );
}

export default connect(
  (state) => ({
      showMood: showMoodSelecter(state),
      user: userSelector(state),
      userData: userDataSelector(state)
  }),
  (dispatch) =>
      bindActionCreators(
          {
              ...modalActions, ...userActions
          },
          dispatch
      )
)(MoodModal);
