import { useState } from "react";
import { Button, Modal } from "react-bootstrap";
import {
  ref as storageRef,
  uploadBytes,
  getDownloadURL,
} from "firebase/storage";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import modalActions from '../../../actions/modal'
import { storage } from "../../../Config/Firebase-config";
import { updateUserAvatar } from "../../../Services/users.Service";
import { showAvatarSelecter } from "../../../reducers/modal";
import { userDataSelector, userSelector } from "../../../reducers/user";
import userActions from '../../../actions/user'

const AvatarModal = ({showAvatar, hideAvatarModal, user, userData, setUserData}) => {
  const [filePath, setFilePath] = useState(null);

  const handleClose = () => hideAvatarModal();

  const updateUploadFile = () => (e) => {
    e.preventDefault();
    setFilePath(e.target?.files[0]);
  };

  const addAvatar = () => async (e) => {
    try {
      e.preventDefault();
      handleClose();
      uploadUserAvatar(userData);
    } catch (e) {
      console.error(e);
    }
  };

  const uploadUserAvatar = (userData) => {
    const file = filePath;

    if (!file) return alert(`Please select a file!`);

    const picture = storageRef(
      storage,
      `users/${userData.userName}/userAvatar`
    );

    uploadBytes(picture, file)
      .then((snapshot) => {
        return getDownloadURL(snapshot.ref).then((url) => {
          setUserData({ userData: {...userData, userAvatar: url}})
          return updateUserAvatar(userData.userName, url);
        });
      })
      .catch(console.error);
  };

  return (
    <Modal show={showAvatar} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Add new avatar</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <div className="mb-3">
          <label htmlFor="formFile" className="form-label">
            File input
          </label>
          <input
            className="form-control"
            type="file"
            id="formFile"
            onChange={updateUploadFile()}
          />
        </div>
      </Modal.Body>
      <Modal.Footer>
        {filePath && (
          <Button variant="secondary" onClick={addAvatar()}>
            Save image
          </Button>
        )}
      </Modal.Footer>
    </Modal>
  );
}

export default connect(
  (state) => ({
      showAvatar: showAvatarSelecter(state),
      user: userSelector(state),
      userData: userDataSelector(state)
  }),
  (dispatch) =>
      bindActionCreators(
          {
              ...modalActions, ...userActions
          },
          dispatch
      )
)(AvatarModal);
