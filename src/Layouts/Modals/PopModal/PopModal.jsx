import { Button, Modal } from "react-bootstrap";

export default function PopModal({
    show,
    onConfirm,
    handleClose,
    bodyText,
    headerText
  }) {
    return (
      <>
        <Modal
          show={show}
          onHide={handleClose}
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>{headerText ? headerText : ''}</Modal.Header>
          <Modal.Body>{bodyText}</Modal.Body>
          <Modal.Footer>
            <Button variant="primary" onClick={onConfirm}>
              Confirm
            </Button>
            <Button variant="secondary" onClick={handleClose}>
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }