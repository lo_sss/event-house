import { useEffect, useState } from "react";
import { Alert, Button, Form, Modal } from "react-bootstrap";
import TextField from "@mui/material/TextField";
import Autocomplete from "@mui/material/Autocomplete";
import {
  fromEventsDocument,
  getLiveEvents,
} from "../../../Services/events.service";
import { addPost, updatePostFileUrl } from "../../../Services/posts.Service";
import React, { useCallback } from "react";
import { useDropzone } from "react-dropzone";
import {
  ref as storageRef,
  uploadBytes,
  getDownloadURL,
} from "firebase/storage";
import { storage } from "../../../Config/Firebase-config";
import { v4 } from "uuid";
import { FileEarmarkArrowUp } from "react-bootstrap-icons";
import {
  supportedImageFormats,
  supportedVideoFormats,
} from "../../../Common/Constants";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import modalActions from '../../../actions/modal'
import { showPostSelecter } from "../../../reducers/modal";
import { userDataSelector } from "../../../reducers/user";
import userActions from '../../../actions/user'
import "./PostModal.css";

const PostModal = ({ showPost, hidePostModal, userData }) => {
  const [post, setPost] = useState({});
  const [events, setEvents] = useState([]);
  const [files, setFiles] = useState([]);
  const [error, setError] = useState(null);

  const onDrop = useCallback((accFiles, rejFiles) => {
    const mappedAcc = accFiles.map((file) => ({ file, errors: [] }));
    mappedAcc.forEach((fileWrapper) => {
      const fileTitle = fileWrapper.file.path.split(".");
      const format = fileTitle[fileTitle.length - 1];

      if (
        !supportedImageFormats.includes(format) &&
        !supportedVideoFormats.includes(format)
      ) {
        setFiles([]);
        const message =
          "Format is not supported. Please add file with format .jpg, .jpeg, .png or .mp4";
        setError({ status: true, message });
        throwError(message);
      }
    });
    setFiles((curr) => [...curr, ...mappedAcc, ...rejFiles]);
  }, []);
  const { getRootProps, getInputProps } = useDropzone({ onDrop });

  const throwError = (message) => {
    throw Error(message);
  };

  const handleClose = () => hidePostModal();

  useEffect(() => {
    const unsubscribe = getLiveEvents((snapshot) => {
      setEvents(fromEventsDocument(snapshot));
    });

    return () => unsubscribe();
  }, []);

  const eventsProps = {
    options: events,
    getOptionLabel: (option) => option.title,
  };

  const updatePost = (title, prop) => {
    setPost({
      ...post,
      [prop]: title,
      author: userData.userName,
      authorAvatar: userData.userAvatar,
    });
  };

  const updatePostEvent = (suggestion) => {
    const foundEvent = events.find((event) => event.title === suggestion.title);

    if (foundEvent) {
      setPost({
        ...post,
        event: foundEvent.uid,
      });
    }
  };

  const createPost = (content) => async (e) => {
    try {
      validatePostContent(content);
      e.preventDefault();
      handleClose();
      const addedPost = await addPost(content);
      setPost({ ...addedPost });
      files.forEach((fileWrapper) => {
        uploadFile(addedPost, fileWrapper.file);
      });
      setFiles([]);
    } catch (e) {
      console.error(e);
    }
  };

  const validatePostContent = (content) => {
    if (!content.event || content.event.lenght === 0) {
      const message = "Please pick event from the list";
      setError({ status: true, message });
      throwError(message);
    }

    if (!content.title) {
      const message = "Please add post";
      setError({ status: true, message });
      throwError(message);
    }
  };

  const uploadFile = (post, file) => {
    const name = v4();
    const fileTitle = file.path.split(".");
    const format = fileTitle[fileTitle.length - 1];
    const picture = storageRef(storage, `posts/${post.uid}/${name}`);

    uploadBytes(picture, file)
      .then((snapshot) => {
        return getDownloadURL(snapshot.ref).then((url) => {
          if (supportedImageFormats.includes(format)) {
            updatePostFileUrl(post.uid, url, name, "images");
          }
          if (supportedVideoFormats.includes(format)) {
            updatePostFileUrl(post.uid, url, name, "videos");
          }
        });
      })
      .catch(console.error);
  };

  return (
    <Modal size="lg" show={showPost} onHide={handleClose}>
      <Modal.Header closeButton>
        <Modal.Title>Add post</Modal.Title>
        <div className="addition">
          (Share your experience from particular event)
        </div>
      </Modal.Header>
      <Modal.Body>
        <Form>
          <Form.Group className="mb-3">
            <Autocomplete
              {...eventsProps}
              id="event"
              getOptionLabel={(option) => (option.title ? option.title : "")}
              onChange={(event, value) => {
                updatePostEvent(value);
              }}
              renderOption={(props, option) => {
                return (
                  <li {...props} key={option.uid}>
                    {option.title}
                  </li>
                );
              }}
              renderInput={(params) => (
                <TextField
                  {...params}
                  sx={{ m: 1, width: "98%" }}
                  label="Attach event"
                />
              )}
            />
            <TextField
              label="Post"
              id="outlined-start-adornment"
              sx={{ m: 1, width: "98%" }}
              onChange={(e) => updatePost(e.target.value, "title")}
            />
            <React.Fragment>
              <p className="guide">
                Drag 'n' drop files or click in the field to select files
              </p>
              <div {...getRootProps()} className="uploadForm">
                <input {...getInputProps()} />
                {files &&
                  files.map((file, key) => (
                    <span key={key}>
                      <FileEarmarkArrowUp className="iconSize" />
                      <span>{`${file.file.path.slice(0, 3)}...${file.file.path
                        .split(".")[0]
                        .slice(-3)}.${file.file.path.split(".")[1]}`}</span>
                    </span>
                  ))}
              </div>
              <div className="note">
                Note: It may take few minutes to show videos in post
              </div>
            </React.Fragment>
          </Form.Group>
        </Form>
      </Modal.Body>
      <Modal.Footer>
        {error && (
          <Alert
            variant="danger"
            className="errorStyle"
            onClose={() => hidePostModal()}
            dismissible
          >
            <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
            <p>{error.message}</p>
          </Alert>
        )}
        <Button variant="secondary" onClick={createPost(post)}>
          Add post
        </Button>
      </Modal.Footer>
    </Modal>
  );
}

export default connect(
  (state) => ({
      showPost: showPostSelecter(state),
      userData: userDataSelector(state)
  }),
  (dispatch) =>
      bindActionCreators(
          {
              ...modalActions, ...userActions
          },
          dispatch
      )
)(PostModal);
