import { Nav } from "react-bootstrap";
import { Link } from "react-router-dom";
import "./Footer.scss";

export default function Footer() {
  return (
    <div className="body">
      <footer className="footer">
        <div className="container">
          <div className="row">
            <div className="col-md-4 footer-column">
              <ul className="nav flex-column">
                <li className="nav-item">
                  <span className="footer-title">Quick links</span>
                </li>
                <li className="nav-item">
                  <Nav.Link className="nav-link" as={Link} to="followed">
                    Followed
                  </Nav.Link>
                </li>
                <li className="nav-item">
                  <Nav.Link className="nav-link" as={Link} to="new-events">
                    Future
                  </Nav.Link>
                </li>
                <li className="nav-item">
                  <Nav.Link className="nav-link" as={Link} to="add-event">
                    Add event
                  </Nav.Link>
                </li>
                <li className="nav-item">
                  <Nav.Link className="nav-link" as={Link} to="search">
                    Search
                  </Nav.Link>
                </li>
              </ul>
            </div>
            <div className="col-md-4 footer-column">
              <ul className="nav flex-column">
                <li className="nav-item">
                  <span className="footer-title">Company</span>
                </li>
                <li className="nav-item">
                  <Nav.Link className="nav-link" as={Link} to="about-us">
                    About us
                  </Nav.Link>
                </li>
                <li className="nav-item">
                  <Nav.Link className="nav-link" as={Link} to="privacy-policy">
                    Privacy policy
                  </Nav.Link>
                </li>
              </ul>
            </div>
            <div className="col-md-4 footer-column">
              <ul className="nav flex-column">
                <li className="nav-item">
                  <span className="footer-title">Contact & Support</span>
                </li>
                <li className="nav-item">
                  <span className="nav-link">
                    <i className="fas fa-phone"></i>+359 000 000 000
                  </span>
                </li>
              </ul>
            </div>
          </div>

          <div className="text-center">
            <i className="fas fa-ellipsis-h"></i>
          </div>

          <div className="row text-center">
            <div className="col-md-4 box"></div>
            <div className="col-md-4 box">
              <span className="copyright quick-links">
                Copyright &copy; Website
              </span>
            </div>
          </div>
        </div>
      </footer>
    </div>
  );
}
