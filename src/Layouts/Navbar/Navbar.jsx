import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import { Link, useNavigate } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import modalActions from "../../actions/modal";
import message from "../../Assets/envelope.png";
import search from "../../Assets/search.png";
import logo from "../../Assets/logo.png";
import { useEffect, useState } from "react";
import { logoutUser } from "../../Services/auth.Service";
import Messages from "../../Components/Messages/Messages";
import { fromUsersDocument, getLiveUsers } from "../../Services/users.Service";
import {
  showAddEventSelecter,
  showAvatarSelecter,
  showMessageSelecter,
  showMoodSelecter,
  showPostSelecter,
} from "../../reducers/modal";
import { userDataSelector, userSelector } from "../../reducers/user";
import userActions from '../../actions/user'
import "./Navbar.css";

const Navmenu = ({
  showAddEvent,
  hideAddEvent,
  hideMoodModal,
  hideAvatarModal,
  hidePostModal,
  showMessageModal,
  hideMessageModal,
  showMes,
  user,
  userData,
  setUser,
  setUserData
}) => {
  const navigate = useNavigate();
  const [messageUser, setMessageUser] = useState(null);
  const [users, setUsers] = useState([]);
  const [correspondences, setCorrespondences] = useState([]);

  const handleShow = () => showAddEvent({ showEvent: true });

  useEffect(() => {
    const unsubscribe = getLiveUsers((snapshot) => {
      const users = fromUsersDocument(snapshot);
      setUsers(users);
      findCorrespondences(userData);
    });

    return () => unsubscribe();
  }, [userData]);

  async function handleLogOut() {
    try {
      await logoutUser()
        .then(() => {
          setUser({user: null})
          setUserData({userData: null})
          hideAddEvent();
          hideAvatarModal();
          hideMoodModal();
          hidePostModal();
          hideMessageModal();
        })
        .catch((error) => console.error(error));

      navigate("/login");
    } catch {
      console.error("Error: Unable to logout");
    }
  }

  const findCorrespondences = (userData) => {
    if (userData && userData.correspondences) {
      setCorrespondences(
        users.filter((user) =>
          Object.keys(userData.correspondences).includes(user.userName)
        )
      );
    }
  };

  const handleShowMessages = (user) => {
    setMessageUser(user);
    showMessageModal({ showMes: true });
  };

  return (
    <>
      {user && userData && users ? (
        <>
          <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Navbar.Brand as={Link} to="/" className="brand">
              <img
                className="thumbnail-image"
                src={logo}
                alt="logo"
                width="150"
                height="40"
              />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
              <Nav className="me-auto">
                <Nav.Link as={Link} to="followed">
                  Followed
                </Nav.Link>
                <Nav.Link as={Link} to="new-events">
                  Future
                </Nav.Link>
                <Nav.Link as={Link} to="add-event" onClick={handleShow}>
                  Add event
                </Nav.Link>
              </Nav>
              <Nav>
                <Nav.Link as={Link} to="search" className="search">
                  <img
                    className="thumbnail-image"
                    src={search}
                    alt="search"
                    width="30"
                    height="30"
                  />
                </Nav.Link>
                <NavDropdown
                  align={{ lg: "end" }}
                  title={
                    <img
                      className="thumbnail-image"
                      src={message}
                      alt="message"
                      width="28"
                      height="30"
                    />
                  }
                  id="collapsible-nav-dropdown-users"
                >
                  {correspondences.length > 0 ? (
                    correspondences.map((user, key) => {
                      return (
                        <NavDropdown.Item
                          key={key}
                          onClick={() => handleShowMessages(user)}
                        >
                          <span>
                            <img
                              className="thumbnail-image avatarImg"
                              src={user.userAvatar}
                              alt="user pic"
                            />
                            {` ${user.userName}`}
                          </span>
                        </NavDropdown.Item>
                      );
                    })
                  ) : (
                    <NavDropdown.Item>No correspondences</NavDropdown.Item>
                  )}
                </NavDropdown>
                <NavDropdown
                  align={{ lg: "end" }}
                  className="user-avatar"
                  title={
                    <img
                      className="thumbnail-image avatarImg"
                      src={userData.userAvatar}
                      alt="user pic"
                    />
                  }
                  id="collasible-nav-dropdown"
                >
                  <NavDropdown.Item
                    as={Link}
                    to={`profile/${userData.userName}`}
                  >
                    Profile
                  </NavDropdown.Item>
                  <NavDropdown.Divider />
                  <NavDropdown.Item onClick={handleLogOut}>
                    Sign out
                  </NavDropdown.Item>
                </NavDropdown>
              </Nav>
            </Navbar.Collapse>
          </Navbar>
          {messageUser && userData && showMes && (
            <Messages
              recipientName={messageUser.userName}
              userName={userData.userName}
            />
          )}
        </>
      ) : (
        <>
          <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Navbar.Brand className="brand">
              <img
                className="thumbnail-image"
                src={logo}
                alt="logo"
                width="150"
                height="40"
              />
            </Navbar.Brand>
            <Nav className="me-auto"></Nav>
            <Nav>
              <Nav.Link as={Link} to="signup" className="sign-up-link">
                Sign up
              </Nav.Link>
            </Nav>
          </Navbar>
        </>
      )}
    </>
  );
};

export default connect(
  (state) => ({
    showEvent: showAddEventSelecter(state),
    showAvatar: showAvatarSelecter(state),
    showMood: showMoodSelecter(state),
    showPost: showPostSelecter(state),
    showMes: showMessageSelecter(state),
    user: userSelector(state),
    userData: userDataSelector(state)
  }),
  (dispatch) =>
    bindActionCreators(
      {
        ...modalActions, ...userActions,
      },
      dispatch
    )
)(Navmenu);
