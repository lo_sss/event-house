import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { Link } from "react-router-dom";

import SingleEvent from "../../Components/Events/SungleEvent/SignleEvent";

export default function SlideShow({ events, itemsCount }) {
  const responsive = {
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: itemsCount,
      slidesToSlide: 3,
      partialVisibilityGutter: 40,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 2,
      slidesToSlide: 2,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
      slidesToSlide: 1,
    },
  };

  return (
    <Carousel
      responsive={responsive}
      swipeable={false}
      draggable={false}
      showDots={false}
      ssr={true}
      infinite={true}
      autoPlaySpeed={7000}
      autoPlay={true}
      keyBoardControl={true}
      transitionDuration={500}
      containerClass="carousel-container"
      removeArrowOnDeviceType={["tablet", "mobile"]}
      dotListClass="custom-dot-list-style"
      itemClass="carousel-item-padding-40-px"
      centerMode={true}
    >
      {!events ? (
        <p>No events to show.</p>
      ) : (
        events.map((event, key) => (
          <Link
            key={key}
            to={`/events/${event.uid}`}
            onClick={() => <SingleEvent />}
          >
            <img src={event.bannerUrl} alt="event" width="95%" height="95%" />
          </Link>
        ))
      )}
    </Carousel>
  );
}
