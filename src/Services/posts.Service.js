import {
  ref,
  push,
  get,
  query,
  equalTo,
  orderByChild,
  update,
  onValue,
  runTransaction,
} from "firebase/database";
import { db } from "../Config/Firebase-config";

export const fromPostsDocument = (snapshot) => {
  const postsDocument = snapshot.val();

  return Object.keys(postsDocument).map((key) => {
    const post = postsDocument[key];

    return {
      ...post,
      uid: key,
      likedBy: post.likedBy ? Object.keys(post.likedBy) : [],
      comments: post.comments ? Object.keys(post.comments) : [],
    };
  });
};

export const addPost = (content) => {
  return push(ref(db, "posts"), {
    ...content,
    createdOn: Date.now(),
    metrics: {
      likes: 0,
      comments: 0,
    },
  })
    .then((result) => {
      return getPostById(result.key);
    })
    .catch(console.error);
};

export const getPostById = (id) => {
  return get(ref(db, `posts/${id}`)).then((result) => {
    if (!result.exists()) {
      throw new Error(`Event with id ${id} does not exist!`);
    }

    const post = result.val();
    post.uid = id;
    post.likedBy = post.likedBy ? Object.keys(post.likedBy) : [];
    post.comments = post.comments ? Object.keys(post.comments) : [];

    return post;
  });
};

export const getPostsByAuthor = (userName) => {
  return get(
    query(ref(db, "posts"), orderByChild("author"), equalTo(userName))
  ).then((snapshot) => {
    if (!snapshot.exists()) return [];

    return fromPostsDocument(snapshot);
  });
};

export const likePost = (userName, postId) => {
  const updateLikes = {};
  updateLikes[`/posts/${postId}/likedBy/${userName}`] = true;
  updateLikes[`/users/${userName}/likedPosts/${postId}`] = true;

  return update(ref(db), updateLikes).then(() => {
    return runTransaction(ref(db, `posts/${postId}`), (post) => {
      post.metrics.likes++;

      return post;
    });
  });
};

export const dislikePost = (userName, postId) => {
  const updateLikes = {};
  updateLikes[`/posts/${postId}/likedBy/${userName}`] = null;
  updateLikes[`/users/${userName}/likedPosts/${postId}`] = null;

  return update(ref(db), updateLikes).then(() => {
    return runTransaction(ref(db, `posts/${postId}`), (post) => {
      post.metrics.likes--;

      return post;
    });
  });
};

export const updatePostFileUrl = (uid, url, name, type) => {
  return update(ref(db), {
    [`posts/${uid}/${type}/${name}`]: url,
  });
};

export const getLivePosts = (listen) => {
  return onValue(ref(db, "posts"), listen);
};

export const getPosts = () => {
  return get(ref(db, `posts`)).then((snapshot) => {
    return fromPostsDocument(snapshot);
  });
};

export const getLikedPosts = (userName) => {
  return get(ref(db, `users/${userName}`)).then((snapshot) => {
    if (!snapshot.val()) {
      throw new Error(`User with handle @${userName} does not exist!`);
    }
    const user = snapshot.val();
    if (!user.likedPosts) return [];

    return Promise.all(
      Object.keys(user.likedPosts).map((key) => {
        return get(ref(db, `posts/${key}`)).then((snapshot) => {
          const post = snapshot.val();

          return {
            ...post,
            createdOn: new Date(post.createdOn),
            uid: key,
            likedBy: post.likedBy ? Object.keys(post.likedBy) : [],
            comments: post.comments ? Object.keys(post.comments) : [],
          };
        });
      })
    );
  });
};

export const deletePost = (postId, likedBy) => {
  return update(ref(db), { [`/posts/${postId}`]: null }).then(() => {
    likedBy.forEach((username) => {
      update(ref(db), {
        [`users/${username}/likedPosts/${postId}`]: null,
      });
    });
  });
};
