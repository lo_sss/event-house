import {
  ref,
  push,
  get,
  update,
  onValue,
  runTransaction,
} from "firebase/database";
import { db } from "../Config/Firebase-config";

export const fromEventsDocument = (snapshot) => {
  const eventsDocument = snapshot.val();

  return Object.keys(eventsDocument).map((key) => {
    const event = eventsDocument[key];

    return {
      ...event,
      uid: key,
      followedBy: event.followedBy ? Object.keys(event.followedBy) : [],
    };
  });
};

export const addEvent = (content) => {
  return push(ref(db, "events"), {
    ...content,
    createdOn: Date.now(),
    metrics: {
      follows: 0,
    },
  })
    .then((result) => {
      return getEventById(result.key);
    })
    .catch(console.error);
};

export const getEventById = (id) => {
  return get(ref(db, `events/${id}`)).then((result) => {
    if (!result.exists()) {
      throw new Error(`Event with id ${id} does not exist!`);
    }

    const event = result.val();
    event.uid = id;
    event.followedBy = event.followedBy ? Object.keys(event.followedBy) : [];

    return event;
  });
};

export const getLiveEvents = (listen) => {
  return onValue(ref(db, "events"), listen);
};

export const updateEventUrl = (uid, url) => {
  return update(ref(db), {
    [`events/${uid}/bannerUrl`]: url,
  });
};

export async function findEventAddress(area, address) {
  const addressUrl = address
    .split(" ")
    .map((element) => "+" + element)
    .join("");

  try {
    const response = await fetch(
      `https://maps.googleapis.com/maps/api/geocode/json?address=+${area},${addressUrl}&key=AIzaSyBt4zfkpWpafR3PqMGCL7LHoXfB5L8EFc4`
    );

    if (!response.ok) {
      throw new Error(`Error! status: ${response.status}`);
    }

    const result = await response.json();
    return result;
  } catch (e) {
    console.error(e);
  }
}

export const followEvent = (userName, eventId) => {
  const updateFollows = {};
  updateFollows[`/events/${eventId}/followedBy/${userName}`] = true;
  updateFollows[`/users/${userName}/followedEvents/${eventId}`] = true;

  return update(ref(db), updateFollows).then(() => {
    return runTransaction(ref(db, `events/${eventId}`), (event) => {
      if (event) event.metrics.follows++;

      return event;
    });
  });
};

export const getFollowedEvents = (userName) => {
  return get(ref(db, `users/${userName}`)).then((snapshot) => {
    if (!snapshot.val()) {
      throw new Error(`User with handle @${userName} does not exist!`);
    }
    const user = snapshot.val();
    if (!user.folloedEvents) return [];

    return Promise.all(
      Object.keys(user.followedEvents).map((key) => {
        return get(ref(db, `events/${key}`)).then((snapshot) => {
          const event = snapshot.val();

          return {
            ...event,
            uid: key,
            followedBy: event.followedBy ? Object.keys(event.followedBy) : [],
          };
        });
      })
    );
  });
};
