import {
  get,
  set,
  ref,
  query,
  equalTo,
  orderByChild,
  update,
  onValue,
} from "firebase/database";
import { db } from "../Config/Firebase-config";

export const getUserByUsername = (userName) => {
  return get(ref(db, `users/${userName}`));
};

export const getUserData = (uid) => {
  return get(query(ref(db, "users"), orderByChild("uid"), equalTo(uid)));
};

export const additionalUserInfo = (userName, uid, email, phoneNumber) => {
  return set(ref(db, `users/${userName}`), {
    userName,
    uid,
    email,
    phoneNumber,
    isBlocked: false,
    userAvatar: "https://pic.onlinewebfonts.com/svg/img_568656.png",
  });
};

export const updateUserAvatar = (userName, url) => {
  return update(ref(db), {
    [`users/${userName}/userAvatar`]: url,
  });
};

export const updateUserMood = (userName, mood) => {
  return update(ref(db), {
    [`users/${userName}/mood`]: mood,
  });
};

export const fromUsersDocument = (snapshot) => {
  const usersDocument = snapshot.val();

  return Object.keys(usersDocument).map((key) => {
    const user = usersDocument[key];
    return { ...user };
  });
};

export const getLiveUsers = (listen) => {
  return onValue(ref(db, "users"), listen);
};
