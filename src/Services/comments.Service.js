import { ref, push, get, update, runTransaction } from "firebase/database";
import { db } from "../Config/Firebase-config";

export const addComment = (postId, author, content, avatar) => {
  return push(ref(db, "comments"), {
    author: author,
    content: content,
    createdOn: Date.now(),
    authorAvatar: avatar,
  }).then((result) => {
    return update(ref(db), {
      [`/posts/${postId}/comments/${result.key}`]: true,
    })
      .then(() => {
        return runTransaction(ref(db, `posts/${postId}`), (post) => {
          post.metrics.comments++;

          return post;
        });
      })
      .then(() => {
        return getCommentById(result.key);
      });
  });
};

export const getCommentById = (id) => {
  return get(ref(db), `comments/${id}`).then((result) => {
    if (!result.exists()) {
      throw new Error(`Comment with the id ${id} does not exist!`);
    }
    const comment = result.val().comments[`${id}`];

    return {
      ...comment,
      createdOn: new Date(comment.createdOn),
    };
  });
};

export const deleteComment = (commentId, postId) => {
  return update(ref(db), { [`comments/${commentId}`]: null }).then(() => {
    return update(ref(db), {
      [`posts/${postId}/comments/${commentId}`]: null,
    }).then(() => {
      return runTransaction(ref(db, `posts/${postId}`), (post) => {
        post.metrics.comments--;
        return post;
      });
    });
  });
};
