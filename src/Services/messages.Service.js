import { ref, update, onValue } from "firebase/database";
import { db } from "../Config/Firebase-config";
import { v4 } from "uuid";

export const addMessage = (userName, recipientName, content) => {
  const updateMessages = {};
  updateMessages[
    `/users/${userName}/correspondences/${recipientName}/${v4()}`
  ] = { ...content, sentByYou: true };
  updateMessages[
    `/users/${recipientName}/correspondences/${userName}/${v4()}`
  ] = { ...content, sentByYou: false };

  return update(ref(db), updateMessages);
};

export const fromCorrespondencesDocument = (snapshot) => {
  const correspondencesDocument = snapshot.val();

  if (correspondencesDocument) {
    return Object.keys(correspondencesDocument).map((key) => {
      const correspondence = correspondencesDocument[key];

      return {
        ...correspondence,
        uid: key,
      };
    });
  }

  return [];
};

export const getLiveMessages = (listen, userName, recipientName) => {
  return onValue(
    ref(db, `users/${userName}/correspondences/${recipientName}`),
    listen
  );
};
