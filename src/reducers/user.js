import { handleActions } from "redux-actions";
import { setUser, setUserData } from "../actions/user";

const initialState = {
    user: null,
    userData: null,
};

export default handleActions(
  {
    [setUser]: (state, action) => {
      return {
        ...state,
        user: action.payload.user,
      };
    },
    [setUserData]: (state, action) => {
      return {
        ...state,
        userData: action.payload.userData,
      };
    },
  },
  initialState
);

export const userSelector = (state) => state?.user?.user;
export const userDataSelector = (state) => state?.user?.userData;

