import { handleActions } from "redux-actions";
import {
  showAddEvent,
  hideAddEvent,
  showAvatarModal,
  hideAvatarModal,
  showMoodModal,
  hideMoodModal,
  showPostModal,
  hidePostModal,
  showMessageModal,
  hideMessageModal,
} from "../actions/modal";

const initialState = {
  modal: {
    showEvent: false,
    showAvatar: false,
    showMood: false,
    showPost: false,
    showMes: false,
  },
};

export default handleActions(
  {
    [showAddEvent]: (state, action) => {
      return {
        ...state.modal,
        showEvent: true,
      };
    },
    [hideAddEvent]: (state, action) => {
      return {
        ...state.modal,
        showEvent: false,
      };
    },
    [showAvatarModal]: (state, action) => {
      return {
        ...state.modal,
        showAvatar: true,
      };
    },
    [hideAvatarModal]: (state, action) => {
      return {
        ...state.modal,
        showAvatar: false,
      };
    },
    [showMoodModal]: (state, action) => {
      return {
        ...state.modal,
        showMood: true,
      };
    },
    [hideMoodModal]: (state, action) => {
      return {
        ...state.modal,
        showMood: false,
      };
    },
    [showPostModal]: (state, action) => {
        return {
          ...state.modal,
          showPost: true,
        };
      },
      [hidePostModal]: (state, action) => {
        return {
          ...state.modal,
          showPost: false,
        };
      },
      [showMessageModal]: (state, action) => {
        return {
          ...state.modal,
          showMes: true,
        };
      },
      [hideMessageModal]: (state, action) => {
        return {
          ...state.modal,
          showMes: false,
        };
      },
  },
  initialState
);

export const modalSelector = (state) => state.modal;
export const showAddEventSelecter = (state) => state.modal.showEvent;
export const showAvatarSelecter = (state) => state.modal.showAvatar;
export const showMoodSelecter = (state) => state.modal.showMood;
export const showPostSelecter = (state) => state.modal.showPost;
export const showMessageSelecter = (state) => state.modal.showMes;
